/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {
  ProjectController: {
    find: ['isLogged'],
    findOne: ['isLogged'],
    create: ['isLogged', 'isAuthenticated'],
    update: ['isLogged', 'isAuthenticated'],
    destroy: ['isLogged', 'isAuthenticated'],
  },
  AuthController: {
    refetchToken: true,
  },
  IssuesController: {
    find: ['isLogged'],
    findOne: ['isLogged'],
    create: ['isLogged', 'isAuthenticated'],
    update: ['isLogged', 'isAuthenticated'],
    destroy: ['isLogged', 'isAuthenticated'],
    filter: ['isLogged'],
  },
  EmailTemplateController: {
    find: ['isLogged'],
    findOne: ['isLogged'],
    create: ['isLogged', 'isAuthenticated'],
    update: ['isLogged', 'isAuthenticated'],
    destroy: ['isLogged', 'isAuthenticated'],
  },
  CategoryController: {
    find: ['isLogged'],
    findOne: ['isLogged'],
    create: ['isLogged', 'isAuthenticated'],
    update: ['isLogged', 'isAuthenticated'],
    destroy: ['isLogged', 'isAuthenticated'],
  },
  UserController: {
    find: ['isLogged'],
    findOne: ['isLogged'],
    create: ['isAuthenticated'],
    update: ['isLogged', 'isAuthenticated'],
    destroy: ['isLogged', 'isAuthenticated'],
  },
  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ***************************************************************************/
  // '*': true,
};
