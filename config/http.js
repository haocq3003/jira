// Trong file config/http.js
// function corsMiddleware(req, res, next) {
//   // Định nghĩa middleware function ở đây
//   optionsCallback(req, (err, options) => {
//     if (err) {
//       return next(err);
//     } else {
//       var corsOptions = assign({}, defaults, options);
//       var originCallback = null;
//       if (corsOptions.origin && typeof corsOptions.origin === 'function') {
//         originCallback = corsOptions.origin;
//       } else if (corsOptions.origin) {
//         originCallback = function (origin, cb) {
//           cb(null, corsOptions.origin);
//         };
//       }

//       if (originCallback) {
//         originCallback(req.headers.origin, (err2, origin) => {
//           if (err2 || !origin) {
//             return next(err2);
//           } else {
//             corsOptions.origin = origin;
//             cors(corsOptions, req, res, next);
//           }
//         });
//       } else {
//         return next();
//       }
//     }
//   });
// }

module.exports.http = {
  middleware: {
    order: ['cookieParser', 'session', 'bodyParser', 'cors'],
    cors: (function () {
      var cors = require('cors');
      return cors();
    })(),
  },
};
