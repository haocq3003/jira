/* eslint-disable linebreak-style */
const errorToken = require('../blueprints/errorToken');
const serverError = require('../blueprints/serverError');
const jwt = require('jsonwebtoken');

module.exports = async function (req, res, next) {
  try {
    const token = req.headers.accesstoken;
    if (token) {
      const accessToken = token.split(' ')[1];
      jwt.verify(accessToken, process.env.SECRETKEY, async (error, user) => {
        if (error) {
          return res.status(401).json(errorToken('Invalid token!'));
        }
        const checkUser = await User.find({ id: user.id });
        if (!checkUser) {
          return res.status(401).json(errorToken('Invalid token!'));
        }
        next();
      });
      // const checkToken = await LoginSession.find({ token: accessToken });
      // if (checkToken) {
      //   return next();
      // } else {
      //   return res.status(402).json(badRequest('Token is not valid'));
      // }
    } else {
      return res.status(402).json(badRequest('You are not logged into the system'));
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json(serverError(500, 'Server error'));
  }
};
