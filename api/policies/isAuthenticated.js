/* eslint-disable linebreak-style */
/* eslint-disable quotes */
var jwt = require('jsonwebtoken');
var dotenv = require('dotenv');
const badRequest = require('../blueprints/badRequest');
dotenv.config();

module.exports = async function (req, res, next) {
  // Lấy token từ yêu cầu
  try {
    const token = req.headers.accesstoken;
    if (token) {
      const accessToken = token.split(' ')[1];
      jwt.verify(accessToken, process.env.SECRETKEY, (error, user) => {
        if (error) {
          return res.status(401).json(badRequest('Token is not valid!'));
        }
        req.user = user;
        // check quyền user

        next();
      });
    } else {
      return res.status(402).json(badRequest("You're not authenticated"));
    }
  } catch (error) {
    // Token không hợp lệ, trả về mã trạng thái 401 Unauthorized
    console.log(error);
    return res.status(401).send({ message: 'Unauthorized' });
  }
};
