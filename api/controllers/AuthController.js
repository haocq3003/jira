const bcrypt = require('bcryptjs');
const { sendEmail } = require('../utils/sendEmail');
const { signupSchema, signinSchema } = require('../schemas/auth');
const notFound = require('../blueprints/notFound');
const serverError = require('../blueprints/serverError');
const badRequest = require('../blueprints/badRequest');
const randomstring = require('randomstring');
const { generateToken } = require('../utils/generateToken');
const { firebase } = require('../../config/firebase');
const errorToken = require('../blueprints/errorToken');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  async register(req, res) {
    try {
      // Validate input data
      const { error } = signupSchema.validate(req.body);
      if (error) {
        return res.json(badRequest(error.details[0].message));
      }

      const { email, password, name } = req.body;
      const nameUser = await User.findOne({ name });
      if (nameUser) {
        return res.json(badRequest('Tên người dùng đã tồn tại, vui lòng nhập tên khác'));
      }

      const emailUser = await User.findOne({ email });
      if (emailUser) {
        return res.json(badRequest('Email đã tồn tại, vui lòng nhập email khác'));
      }
      // Hash password
      const hashedPassword = await bcrypt.hash(password, 10);

      const newUser = await User.create({
        name: name,
        email: email,
        password: hashedPassword,
      }).fetch();

      return res.ok({
        error: false,
        message: 'Đăng ký thành công',
        datas: newUser,
      });
    } catch (error) {
      res.status(500).json(serverError(error.message));
    }
  },
  async verifyOtp(req, res) {
    try {
      const { email, otpCode } = req.body;
      const otpForUser = await Otps.findOne({ email });

      if (otpForUser.otp !== otpCode) {
        return res.status(400).json(badRequest('Mã xác minh không hợp lệ'));
      } else if (otpForUser.expiresAt < Date.now()) {
        return res.status(400).json(badRequest('Mã xác minh đã hết hạn'));
      } else {
        return res.status(200).ok({
          error: false,
          message: 'Tài khoản đã xác minh thành công',
        });
      }
    } catch (error) {
      console.log(error);
      return res.status(500).json(serverError(error.message));
    }
  },
  async refetchOtpCode(req, res) {
    try {
      const { email } = req.body;
      const otpForUser = await Otps.findOne({ email });
      const otpCode = randomstring.generate({ length: 6, charset: 'numeric' });
      const otpExpiration = Date.now() + 2 * 60 * 1000;

      if (otpForUser) {
        await Otps.updateOne({ id: otpForUser.id }).set({ otp: otpCode, expiresAt: otpExpiration });
      }

      sendEmail(otpCode, email);
      return res.status(200).ok({
        error: false,
        message: 'Refetch mã otp thành công!',
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json(serverError(error.message));
    }
  },
  async login(req, res) {
    try {
      // Validate input data
      const { error } = signinSchema.validate(req.body);
      if (error) {
        return res.json(badRequest(error.details[0].message));
      }
      const { email, password } = req.body;

      // Check if email exists
      const user = await User.findOne({ email });
      if (!user) {
        return res.json(notFound('Email does not exist'));
      }

      // Check if password is correct
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        return res.json(badRequest('Invalid password'));
      }

      // create token
      const token = await generateToken({ id: user.id, name: user.name, email: user.email });

      // create otp
      const otpCode = randomstring.generate({ length: 6, charset: 'numeric' });
      const otpExpiration = Date.now() + 2 * 60 * 1000;

      const checkOtp = await Otps.findOne({ email });
      if (checkOtp) {
        await Otps.updateOne({ id: checkOtp.id }).set({ otp: otpCode, expiresAt: otpExpiration });
      } else {
        await Otps.create({ email, otp: otpCode, expiresAt: otpExpiration });
      }

      sendEmail(otpCode, email);
      return res.ok({
        error: false,
        message: 'Đăng nhập thành công',
        accessToken: token,
      });
    } catch (error) {
      console.log(error);
      res.status(500).json(serverError(error.message));
    }
  },
  async loginWithGoogle(req, res) {
    try {
      const idToken = req.body.idToken;
      const decodedToken = await firebase.auth().verifyIdToken(idToken);
      let checkUser = await User.findOne({ email: decodedToken.email });
      if (checkUser) {
        const token = await generateToken({ id: checkUser.id, name: checkUser.name, email: checkUser.email });
        return res.ok({
          error: false,
          message: 'Đăng nhập thành công',
          accessToken: token,
        });
      } else {
        const newUser = await User.create({
          email: decodedToken.email,
          name: decodedToken.name,
        }).fetch();
        const token = await generateToken({ id: newUser.id, name: newUser.name, email: newUser.email });
        return res.ok({
          error: false,
          message: 'Đăng nhập thành công',
          accessToken: token,
        });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json(serverError(error.message));
    }
  },
  async refetchToken(req, res) {
    try {
      const refreshToken = req.body.refreshToken;
      const decodedToken = jwt.verify(refreshToken, process.env.SECRETKEY);

      const user = await User.findOne({ id: decodedToken.id });
      if (!user) {
        return res.json(errorToken('Invalid token'));
      }

      const accessToken = await generateToken({ id: user.id, name: user.name, email: user.email });

      return res.ok({
        error: false,
        message: 'Refetch token thành công',
        accessToken,
      });
    } catch (err) {
      console.log(err);
      res.status(500).json(serverError(err.message));
    }
  },
};
