module.exports = {
  find: async (req, res) => {
    const db = Issues.getDatastore().manager;

    const resultGetIssues = await db
      .collection('issues')
      .aggregate([
        { $unwind: '$assignees' },
        { $addFields: { idUser: { $toObjectId: '$assignees' } } },
        {
          $lookup: {
            from: 'user',
            localField: 'idUser',
            foreignField: '_id',
            as: 'assigns',
          },
        },
        {
          $group: {
            _id: '$_id',
            assigns: { $push: { $arrayElemAt: ['$assigns', 0] } },
            title: { $first: '$title' },
            topic: { $first: '$topic' },
            description: { $first: '$description' },
            status: { $first: '$status' },
            priority: { $first: '$priority' },
            reporter: { $first: '$reporter' },
            assignees: { $push: '$idUser' },
            createdAt: { $first: '$createdAt' },
          },
        },
        {
          $project: {
            title: 1,
            topic: 1,
            description: 1,
            status: 1,
            priority: 1,
            reporter: 1,
            createdAt: 1,
            assignees: 1,
            assigns: {
              _id: 1,
              name: 1,
            },
          },
        },
        {
          $sort: {
            createdAt: 1,
          },
        },
      ])
      .toArray();

    const formatResultGetIssues = resultGetIssues.map((value) => {
      const { _id, assigns, ...otherData } = value;
      return {
        id: _id,
        assigns: assigns.map((assign) => {
          return {
            id: assign._id,
            name: assign.name,
          };
        }),
        ...otherData,
      };
    });
    return res.status(200).json(formatResultGetIssues);
  },
  filter: async (req, res) => {
    const db = Issues.getDatastore().manager;
    const { title, assignees, ...queries } = req.query;

    const matchQuery = {
      ...queries,
    };

    if (assignees) {
      matchQuery.assignees = { $in: Array.from(assignees.split(',')) };
    }

    if (title) {
      matchQuery.title = { $regex: `.*${title}.*` };
    }

    const resultFilterIssues = await db
      .collection('issues')
      .aggregate([
        {
          $match: matchQuery,
        },
        { $unwind: '$assignees' },
        { $addFields: { idUser: { $toObjectId: '$assignees' } } },
        {
          $lookup: {
            from: 'user',
            localField: 'idUser',
            foreignField: '_id',
            as: 'assigns',
          },
        },
        {
          $group: {
            _id: '$_id',
            assigns: { $push: { $arrayElemAt: ['$assigns', 0] } },
            title: { $first: '$title' },
            topic: { $first: '$topic' },
            description: { $first: '$description' },
            status: { $first: '$status' },
            priority: { $first: '$priority' },
            reporter: { $first: '$reporter' },
            assignees: { $push: '$idUser' },
            createdAt: { $first: '$createdAt' },
          },
        },
        {
          $project: {
            title: 1,
            topic: 1,
            description: 1,
            status: 1,
            priority: 1,
            reporter: 1,
            assignees: 1,
            createdAt: 1,
            assigns: {
              _id: 1,
              name: 1,
            },
          },
        },
        {
          $sort: {
            createdAt: 1,
          },
        },
      ])
      .toArray();
    const formatResultFilterIssues = resultFilterIssues.map((value) => {
      const { _id, ...otherData } = value;
      return {
        id: _id,
        ...otherData,
      };
    });

    return res.json(formatResultFilterIssues);
  },
};
