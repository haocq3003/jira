/* eslint-disable linebreak-style */
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const dotenv = require('dotenv');
dotenv.config();

const generateToken = async (data) => {
  const token = jwt.sign(data, process.env.SECRETKEY, {
    expiresIn: '1h',
  });
  // const token = crypto.randomBytes(32).toString('hex');
  // await LoginSession.create({
  //   userId: data.id,
  //   token,
  // });
  return token;
};

module.exports = { generateToken };
