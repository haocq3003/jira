/**
 * Issues.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    id: { type: 'string', columnName: '_id' },

    topic: { type: 'string', required: true },

    title: { type: 'string', required: true },

    description: { type: 'string', required: true },

    status: { type: 'string', required: true },

    assignees: { type: 'json', columnType: 'array' },

    priority: { type: 'string', required: true },

    originalEstimate: { type: 'number' },

    reporter: { type: 'string', required: true },

    startTime: {
      type: 'ref', // lưu trữ giá trị bất kỳ, như là một Date object
      columnType: 'datetime',
    },
    dueTime: { type: 'ref', columnType: 'datetime' },
    createdAt: { type: 'number', autoCreatedAt: true },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
  },
};
