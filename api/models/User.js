/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: 'default',
  tableName: 'user',
  dontUseObjectIds: false,
  attributes: {
    id: {
      type: 'string',
      columnName: '_id',
    },
    name: {
      type: 'string',
      required: true,
    },
    email: {
      type: 'string',
      required: true,
      unique: true,
      isEmail: true,
      maxLength: 255,
    },
    password: {
      type: 'string',
      minLength: 6,
      maxLength: 255,
    },
    otpCode: {
      type: 'string',
    },
    otpExpiration: {
      type: 'number',
    },
  },
};
